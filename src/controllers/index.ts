export * from './ping.controller';
export * from './teachers.controller';
export * from './students.controller';
export * from './teacher-student.controller';
