import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {RelationsDataSource} from '../datasources';
import {Teacher, TeacherRelations, Student} from '../models';
import {StudentRepository} from './student.repository';

export class TeacherRepository extends DefaultCrudRepository<
  Teacher,
  typeof Teacher.prototype.id,
  TeacherRelations
> {

  public readonly teacherStudentRelation: HasManyRepositoryFactory<Student, typeof Teacher.prototype.id>;

  constructor(
    @inject('datasources.Relations') dataSource: RelationsDataSource, @repository.getter('StudentRepository') protected studentRepositoryGetter: Getter<StudentRepository>,
  ) {
    super(Teacher, dataSource);
    this.teacherStudentRelation = this.createHasManyRepositoryFactoryFor('teacherStudentRelation', studentRepositoryGetter,);
    this.registerInclusionResolver('teacherStudentRelation', this.teacherStudentRelation.inclusionResolver);
  }
}
