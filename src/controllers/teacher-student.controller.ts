import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Teacher,
  Student,
} from '../models';
import {TeacherRepository} from '../repositories';

export class TeacherStudentController {
  constructor(
    @repository(TeacherRepository) protected teacherRepository: TeacherRepository,
  ) { }

  @get('/teachers/{id}/students', {
    responses: {
      '200': {
        description: 'Array of Teacher has many Student',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Student)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Student>,
  ): Promise<Student[]> {
    return this.teacherRepository.teacherStudentRelation(id).find(filter);
  }

  @post('/teachers/{id}/students', {
    responses: {
      '200': {
        description: 'Teacher model instance',
        content: {'application/json': {schema: getModelSchemaRef(Student)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Teacher.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Student, {
            title: 'NewStudentInTeacher',
            exclude: ['stud_id'],
            optional: ['teacherId']
          }),
        },
      },
    }) student: Omit<Student, 'stud_id'>,
  ): Promise<Student> {
    return this.teacherRepository.teacherStudentRelation(id).create(student);
  }

  @patch('/teachers/{id}/students', {
    responses: {
      '200': {
        description: 'Teacher.Student PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Student, {partial: true}),
        },
      },
    })
    student: Partial<Student>,
    @param.query.object('where', getWhereSchemaFor(Student)) where?: Where<Student>,
  ): Promise<Count> {
    return this.teacherRepository.teacherStudentRelation(id).patch(student, where);
  }

  @del('/teachers/{id}/students', {
    responses: {
      '200': {
        description: 'Teacher.Student DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Student)) where?: Where<Student>,
  ): Promise<Count> {
    return this.teacherRepository.teacherStudentRelation(id).delete(where);
  }
}
